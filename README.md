# IOTA Crypto Core FPGA Module

This is the third milestone of the ICCFPGA project: https://gitlab.com/iccfpga/iccfpga-core/wikis/home


The FPGA module can do the following:

- integrated Cortex M1 (32Bit ARM with 100MHz clock) which is programmable in C/C++. It also can be debugged via a standard debugging interface (SWD).
- it has accelerators for Trinary <-> Binary type conversions, Hashing (CURL-P81, KECCAC384, Troika; single clock-cycle per hashing-round) and can do Proof-of-Work very fast (~330ms avg). 
- it does Seed management and can store up to 8 seeds in a secure memory
- it provides a JSON-Api which can be used via RS232. Currently it offers commands, for generating Addresses or random Seeds, signing Transactions and PoW - everything hardware accelerated.
- everything runs contained in the FPGA and the configuration file of the FPGA can be encrypted. It's very easy to update the FPGA configuration but it's impossible* to tamper with the microprocessor-code (which is embedded as ROM) or to extract sensitive information like keys.
- it has integrated peripherals like RS232 or SPI which are routed to the mini PCIe connector and offers spare (unused) pins at the upper side of the module. It also provides additional digital inputs and outputs on the module connector.

*: There is no "impossible" when it comes to security. But the encryption algorithm AES128 is unbroken.